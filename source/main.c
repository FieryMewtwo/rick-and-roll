// Include some necessary headers from C's standard library:
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Include the main libnx system header, for Switch development. (important!)
#include <switch.h>

// Main program entrypoint:
int main(int argc, char* argv[])
{
    consoleInit(NULL);

    // Configure our supported input layout: a single player with standard controller styles
    padConfigureInput(1, HidNpadStyleSet_NpadStandard);

    // Initialize the default gamepad (which reads handheld mode inputs as well as the first connected controller)
    PadState pad;
    padInitializeDefault(&pad);

    // Main loop
    while (appletMainLoop())
    {
        // Scan the gamepad. This should be done once for each frame
        padUpdate(&pad);

        // padGetButtonsDown returns the set of buttons that have been
        // newly pressed in this frame compared to the previous one
        u64 kDown = padGetButtonsDown(&pad);

        if (kDown & HidNpadButton_Plus) // Has the Plus button been pressed?
            break; // break in order to return to hbmenu
        
        if (kDown & HidNpadButton_Minus) // Has the Minus button pressed?
            break; // break in order to return to hbmenu
        
        printf ("Arch is the best!")

        // Update the console, sending a new frame to the display
        consoleUpdate(NULL);
    }

    // Deinitialize and clean up resources used by the console (important!)
    consoleExit(NULL);
    return 0;
}

// This code is largely stolen from the examples by @yellows8 on GitHub. :shameless-emoji
// If this code breaks, bug me on Riot: fierymewtwo:matrix.org
