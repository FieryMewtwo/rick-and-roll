# Rick and Roll

Rick and Roll is a homebrew application for Nintendo Switch® consoles running [Atmosphère](https://github.com/AtmosphereNX/Atmosphere) custom firmware and `hbmenu`

To play, download `rick-and-roll.nro` from the Releases section and copy it to the `switch` folder on the microSD card.

Useless Text is also being prepped to be released to the [4TU `hbappstore`](apps.fortheusers.org/switch). When that happens, use it instead of Releases.

To compile  on your machine, see [COMPILING.md](COMPILING.md)
)
